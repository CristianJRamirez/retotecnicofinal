package com.choucair.reto.definition;

import java.util.List;

import com.choucair.reto.steps.CreacionBusinessUnitSteps;
import com.choucair.reto.steps.LoginSerenitySteps;
import com.choucair.reto.steps.ProgramarReunionSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.*;
import net.thucydides.core.annotations.Steps;

public class CreacionReunionDefinition {

	@Steps
	ProgramarReunionSteps programarreunionsteps;

	@Steps
	LoginSerenitySteps loginserenitysteps;
	
	@Steps
	CreacionBusinessUnitSteps creacionbusinessunitsteps;

	@Given("^ingreso a la plataforma con las credenciales \"([^\"]*)\" y pass \"([^\"]*)\"$")
	public void ingreso_a_la_plataforma_con_las_credenciales_y_pass(String username, String password) {
		loginserenitysteps.genera_nombre_unidad();
		loginserenitysteps.open();
		loginserenitysteps.ingresar_datos(username, password);
	}

	@When("^Selecciono la opcion unidades de negocio$")
	public void selecciono_la_opcion_unidades_de_negocio() {
		creacionbusinessunitsteps.seccion_unidad_negocio();
	}


	@When("^Creo una nueva unidad de negocio \"([^\"]*)\"$")
	public void creo_una_nueva_unidad_de_negocio(String dato) {
		creacionbusinessunitsteps.agregar_unidad_negocio(dato);

	}

	@Then("^Valido que se haya creado la unidad de negocio$")
	public void valido_que_se_haya_creado_la_unidad_de_negocio() {

	}

	@When("^Selecciono la opcion meetings y new meeting$")
	public void selecciono_la_opcion_meetings_y_new_meeting() {
		programarreunionsteps.seccion_reunion();
	}

	@When("^Creo una nueva reunion$")
	public void creo_una_nueva_reunion(DataTable datos) {
		List<List<String>> data = datos.raw();
		programarreunionsteps.unit(null);

		for (int i = 1; i < data.size(); i++) {
           programarreunionsteps.diligenciar_formulario(data, i);
		}
	}

	@Then("^Valido que se haya creado la reunion$")
	public void valido_que_se_haya_creado_la_reunion() {
		programarreunionsteps.validar_reunion();

	}

}
