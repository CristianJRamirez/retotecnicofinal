package com.choucair.reto.steps;

import com.choucair.reto.pageobjects.LoginSerenityPage;
import com.choucair.reto.utilities.AccionesWeb;

import net.thucydides.core.annotations.Step;

public class LoginSerenitySteps {
	
	LoginSerenityPage loginserenitypageobject;
	AccionesWeb accionesweb;
	
	@Step
	public void open() {
		accionesweb.abrirURL("https://serenity.is/demo/Account/Login/?ReturnUrl=%2Fdemo%2F");
		accionesweb.espera_implicita(2);
		
	}
	
	@Step
	public void ingresar_datos(String username, String password) {
		accionesweb.bordearElemento(loginserenitypageobject.txtUsername);
		accionesweb.clear_sendKeys(loginserenitypageobject.txtUsername, username,false);
		accionesweb.bordearElemento(loginserenitypageobject.txtPassword);
		accionesweb.clear_sendKeys(loginserenitypageobject.txtPassword, password,false);
		accionesweb.espera_implicita(1);
		accionesweb.bordearElemento(loginserenitypageobject.btnSignIn);
		accionesweb.click(loginserenitypageobject.btnSignIn, false);

	}
	
	@Step
	public void valido_inicio_sesion() {
		accionesweb.bordearElemento(loginserenitypageobject.navMenu);
     accionesweb.esperoElementoVisible(loginserenitypageobject.navMenu);
	}

	public void genera_nombre_unidad() {
		accionesweb.lectura_archivo("nombre.txt");
		
	}
}
