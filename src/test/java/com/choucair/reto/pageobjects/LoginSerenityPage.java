package com.choucair.reto.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://serenity.is/demo/Account/Login/?ReturnUrl=%2Fdemo%2F")

public class LoginSerenityPage extends PageObject {

	// campo usuario del login
	@FindBy(id ="StartSharp_Membership_LoginPanel0_Username")
	public WebElementFacade txtUsername;

	// campo contraseña del login
	@FindBy(id = "StartSharp_Membership_LoginPanel0_Password")
	public WebElementFacade txtPassword;

	// Boton iniciar sesion
	@FindBy(id = "StartSharp_Membership_LoginPanel0_LoginButton")
	public WebElementFacade btnSignIn;

	// texto home
	@FindBy(xpath = "//NAV[@class='navbar navbar-static-top']")
	public WebElementFacade navMenu;

}
