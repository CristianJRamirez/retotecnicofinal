@plataformaReuniones
Feature: Programacion de reuniones
  El sistema debe permitir crear una unidad de negocio y crear una reunion para esta
 
@CreaUnidadNegocio
Scenario: Creacion de la unidad de negocio
					El usuario debe realizar la creacion de una unidad de negocios en la platforma
  	Given ingreso a la plataforma con las credenciales "admin" y pass "serenity"
		When Selecciono la opcion unidades de negocio
		And Creo una nueva unidad de negocio "unidad3"
    Then Valido que se haya creado la unidad de negocio
  
  @CreaLlamada
  Scenario: programacion da la reunion para unidad de negocio
 	 				El sistema debe permitir la creacion de la reunion de la unidad de negocio 
 	 	Given ingreso a la plataforma con las credenciales "admin" y pass "serenity"
		When Selecciono la opcion meetings y new meeting
		And Creo una nueva reunion 
		|Meeting Name |Meeting Number|
    |Reunion      | 123434545    |
    Then Valido que se haya creado la reunion